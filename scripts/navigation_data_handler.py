import rr
import json
from pygame import mixer
import datetime

mixer.init()
currentTime = datetime.datetime.now()

imageWidth = rr.GetVariable("IMAGE_WIDTH");
imageHeight = rr.GetVariable("IMAGE_HEIGHT");
imageCenter = imageWidth / 2;

objectXCoord = rr.GetFloatVariable("OBJECT_X_COORD");
objectYCoord = rr.GetFloatVariable("OBJECT_Y_COORD");


objectCount = rr.GetVariable("OBJECT_COUNT");

centerObjXCoord = 0
centerObjYCoord = 0
sideObjXCoord = 0
sideObjYCoord = 0
sideObjXCoordList = []
sideObjYCoordList = []
objects = rr.GetArrayVariable("OBJECTS");
objectsPath = rr.GetStrVariable("OBJECTS_PATH");
isCenterObjectDetected = False
TYPE_CENTER_OBJECT = 'pos_1'
TYPE_SIDE_OBJECT = 'pos_2'


objectOrientation = rr.GetFloatVariable("OBJECT_ORIENTATION");

imageCount = rr.GetVariable("IMAGE_COUNT");

prevObjectXCoord = rr.GetFloatVariable("PREV_OBJECT_X_COORD");
prevObjectYCoord = rr.GetFloatVariable("PREV_OBJECT_Y_COORD");

lastCoordUpdateTime = rr.GetVariable("LAST_COORD_UPDATE_TIME");

obstacleStatusData = rr.GetStrVariable("OBSTACLE_STATUS_JSON");
previousObstacleDataSequenceNum = rr.GetVariable("PREV_OBSTACLE_DATA_SEQ_NUM");
clockTime = rr.GetVariable("CLOCK_TIME");
lastTurnTime = rr.GetVariable("LAST_TURN_TIME");

turningLeft = rr.GetVariable("IS_TURNING_LEFT");
turningRight = rr.GetVariable("IS_TURNING_RIGHT");
turningBack = rr.GetVariable("IS_TURNING_BACK");

turningLeftTowardsObstacle = rr.GetVariable("IS_TURNING_LEFT_TOWARDS_OBSTACLE");
turningRightTowardsObstacle = rr.GetVariable("IS_TURNING_RIGHT_TOWARDS_OBSTACLE");
turnTowardsObstacleStartTime = rr.GetVariable("TURN_TOWARDS_OBSTACLE_START_TIME");
isObstacleOnFront = rr.GetVariable("IS_OBSTACLE_ON_FRONT");
#isObstacleOnFront = 0
lastObstacleDetectTime = rr.GetVariable("LAST_OBSTACLE_DETECT_TIME");

#turningLeft = 0;
#turningRight = 0;
#turningBack = 0;
turnStatusJson = rr.GetStrVariable("TURN_STATUS_JSON");

lastTurnPosition = rr.GetVariable("LAST_TURN_POSITION")
scriptCount = rr.GetVariable("SCRIPT_COUNT");
isObstacleDetected = rr.GetVariable("IS_OBSTACLE_DETECTED");
previousTurnStatusSequenceNum = rr.GetVariable("PREV_TURN_STATUS_SEQ_NUM");
lastObjectTrackedTime = rr.GetVariable("LAST_OBJECT_TRACKED_TIME");
lastSerialInputReceivedTime = rr.GetVariable("LAST_SERIAL_INPUT_RECEIVED_TIME");
lastWelcomeVoicePlayedTime = rr.GetVariable("LAST_AUDIO_PLAYED_TIME");


#isThankYouVoicePlaying =  rr.GetVariable("IS_THANK_YOU_PLAYING");
isWelcomeVoicePlaying = rr.GetVariable("IS_WELCOME_VOICE_PLAYING");

previousObstacleDetectionState = rr.GetVariable("PREV_OBSTACLE_DETECT_STATE")
isObstacleDetected = rr.GetVariable("IS_OBSTACLE_DETECTED");
isSensorDefective = rr.GetVariable("IS_SENSOR_DEFECTIVE");
#isSensorDefective = 0
previousObstaclePosition = rr.GetStrVariable("PREVIOUS_OBSTACLE_POSITION");

songStarted = rr.GetVariable("SONG_STARTED");

#print "previousObstaclePosition: " + previousObstaclePosition

obstaclePosition = "NA"
isObstacleOnFrontCenter = 0;
OBSTACLE_POSITION_FRONT = "FRONT";
OBSTACLE_POSITION_BACK = "BACK";
OBSTACLE_POSITION_NA = "NA";

MAX_OBJECT_TRACK_IDLE_PERIOD = 5 #seconds
MAX_SERIAL_INPUT_TIMEOUT = 10 #seconds
MAX_AUDIO_PLAYBACK_TIME = 4 #seconds
WELCOME_VOICE_PATH = 'audio/welcome_to_ea_female.mp3'
SONG_PATH =  'audio/song.mp3'
THANK_YOU_AUDIO_PATH = 'audio/thank_you.mp3'

if currentTime.hour >= 17:
	SONG_PATH = 'audio/evening_songs.mp3'
else:
	SONG_PATH = 'audio/morning_songs.mp3'
	
BOTTOM_POSITION = 1
TOP_POSITION = 2
DIRECTION_LEFT = 'LEFT'
DIRECTION_RIGHT = 'RIGHT'
DIRECTION_FORWARD = 'FORWARD'
DIRECTION_STOP = 'STOP'
DIRECTION_TURN_BACK = 'TURN_BACK'
DIRECTION_LEFT_TOWARDS_OBSTACLE = 'LEFT_TOWARDS_OBSTACLE'
DIRECTION_RIGHT_TOWARDS_OBSTACLE = 'RIGHT_TOWARDS_OBSTACLE'

JSON_TYPE = 'DIRECTION'
MIN_X_EDGE_DIFFERENCE_FOR_TURN = 40
MIN_Y_EDGE_DIFFERENCE_FOR_TURN = 60

SONG_FADEOUT_MILLIS = 1000

SENSOR_POS_UPPER_FRONT_CENTER = "T1"
SENSOR_POS_UPPER_FRONT_RIGHT = "T2"
SENSOR_POS_UPPER_BACK_RIGHT = "T3"
SENSOR_POS_UPPER_BACK_CENTER = "T4"
SENSOR_POS_UPPER_BACK_LEFT = "T5"
SENSOR_POS_UPPER_FRONT_LEFT = "T6"

SENSOR_POS_MIDDLE_CENTER = "M1"
SENSOR_POS_MIDDLE_RIGHT = "M2"
SENSOR_POS_MIDDLE_LEFT = "M3"

SENSOR_POS_LOWER_FRONT_CENTER = "B1"
SENSOR_POS_LOWER_FRONT_RIGHT_1 = "B2"
SENSOR_POS_LOWER_FRONT_LEFT_2 = "B3"
SENSOR_POS_LOWER_BACK_RIGHT = "B4"
SENSOR_POS_LOWER_BACK_CENTER = "B5"
SENSOR_POS_LOWER_BACK_LEFT = "B6"
SENSOR_POS_LOWER_FRONT_RIGHT_2 = "B7"
SENSOR_POS_LOWER_FRONT_LEFT_1 = "B8"

directionData = {};
directionData['d_type'] = JSON_TYPE
directionData['direction'] = DIRECTION_FORWARD
directionDataJson = ''
targetTurnPosition = rr.GetVariable("TARGET_TURN_POSITION");
turnCount = rr.GetVariable("TURN_COUNT");



for index in xrange(0, len(objects)-1, 15):
	objX2 = objects[index + 5]
	objY2 = objects[index + 6]
	objX1 = objects[index + 1]
	objY1 = objects[index + 2]
	#print "objX1: " + str(objX1) + ", objY1: " + str(objY1)
	#print "objX2: " + str(objX2) + ", objY2: " + str(objY2)
	objX = (objX1 + objX2) / 2;
	objY = (objY1 + objY2) / 2;
	objTypeStartIndex = int(objects[index + 13])
	#print objTypeStartIndex 
	objType = objectsPath[objTypeStartIndex : objTypeStartIndex + 5]
	#print "Category: " + objType 
	if objType == TYPE_CENTER_OBJECT:
		if ((imageHeight - objY) > MIN_Y_EDGE_DIFFERENCE_FOR_TURN) or (objectCount == 1) :
			isCenterObjectDetected = True
			centerObjXCoord = objX 
			centerObjYCoord = objY
		else:
			isCenterObjectDetected = False

	elif objType == TYPE_SIDE_OBJECT:
		#print "objX: " + str(objX)
		#print "objY: " + str(objY)
		if sideObjYCoord == 0:
			
			sideObjXCoord = objX 
			sideObjYCoord = objY
		else:
			if objY < sideObjYCoord :
				sideObjXCoord = objX 
				sideObjYCoord = objY
				
if isCenterObjectDetected:
	objectXCoord = centerObjXCoord 
	objectYCoord = centerObjYCoord 
	
else:
	objectXCoord = sideObjXCoord 
	objectYCoord = sideObjYCoord 
	MIN_X_EDGE_DIFFERENCE_FOR_TURN = 50
	MIN_Y_EDGE_DIFFERENCE_FOR_TURN = 80
	
#print isCenterObjectDetected 		
print "X: " + str(objectXCoord) + ", Y: "+ str(objectYCoord)

if scriptCount == 1:
	rr.SetVariable("SONG_STARTED", 0);
	rr.SetVariable("CURRENT_SONG_POSITION", 0)
	mixer.music.stop()

if not songStarted:
	print "Start song from beginning"		
	mixer.music.load(SONG_PATH)
	mixer.music.play()
	songStarted = 1
	isSongPlaying = 1
	rr.SetVariable("SONG_START_TIME", clockTime)
	
if turningLeftTowardsObstacle:
	directionData['direction'] = DIRECTION_LEFT_TOWARDS_OBSTACLE

elif turningRightTowardsObstacle:
	directionData['direction'] = DIRECTION_RIGHT_TOWARDS_OBSTACLE


elif turningLeft or turningRight or turningBack:
	turnStatusCount = 0
	if turningLeft:
		directionData['direction'] = DIRECTION_LEFT
	elif turningRight:
		directionData['direction'] = DIRECTION_RIGHT
	else:
		directionData['direction'] = DIRECTION_TURN_BACK


	print "Turning"
	try:
		print turnStatusJson
		turnStatusJson = ""
		turnStatusData = json.loads(turnStatusJson)
		
		turnStatus = turnStatusData['status']
		turnStatusCount = turnStatusData['count']
		if (turnStatusCount != previousTurnStatusSequenceNum and turnStatus == "COMPLETED"):
			print "Reached target"
			lastTurnTime = clockTime
			
			directionData['direction'] = DIRECTION_FORWARD
			turningRight = 0
			turningLeft = 0
			turningBack = 0;
	except ValueError as e:
		print "parse error"
		pass
	previousTurnStatusSequenceNum = turnStatusCount 
else:			
	if objectCount > 0:
		lastObjectTrackedTime = clockTime 
		
		if (clockTime - lastTurnTime) >= 5 :
			
			if (objectXCoord < MIN_X_EDGE_DIFFERENCE_FOR_TURN):
				print "x edge, turn left" 
				directionData['direction'] = DIRECTION_LEFT
				turningLeft = 1

			elif ((imageWidth - objectXCoord) < MIN_X_EDGE_DIFFERENCE_FOR_TURN):
				print "x edge, right" 
				directionData['direction'] = DIRECTION_RIGHT
				turningRight = 1

				
			elif ((imageHeight - objectYCoord) < MIN_Y_EDGE_DIFFERENCE_FOR_TURN) :
				if objectXCoord <= imageCenter: 
					print "y edge left" 
					
					directionData['direction'] = DIRECTION_LEFT	
					turningLeft = 1

				else:
					print "right" 
					directionData['direction'] = DIRECTION_RIGHT
					turningRight = 1
					
			else:
				#robot is in the middle/on track, no need for turning
				if not turningLeft and not turningRight:
					print "On track"
					directionData['direction'] = DIRECTION_FORWARD
					turningRight = 0
					turningLeft = 0
		else:
			directionData['direction'] = DIRECTION_FORWARD
			turningRight = 0
			turningLeft = 0
		
	else:
		print "no object detected"
		#if not turningLeft and not turningRight:
			#Stop if no object was detected for more than 5 seconds and not turning
		if (clockTime - lastObjectTrackedTime) > MAX_OBJECT_TRACK_IDLE_PERIOD:
			directionData['direction'] = DIRECTION_STOP




if (len(obstacleStatusData) > 0):
	
	try:
		obstacleDataJson = json.loads(obstacleStatusData)
		obstacleDataSequenceNum = obstacleDataJson['seq_num']
		obstaclePosition = str(obstacleDataJson['obstacle_pos'])
		if not isSensorDefective :
			isSensorDefective = obstacleDataJson['is_sensor_defective']
		#print obstaclePosition 	

		if (obstacleDataSequenceNum != previousObstacleDataSequenceNum):
			lastSerialInputReceivedTime = clockTime
			isObstacleDetected = obstacleDataJson['is_obstacle_present']
			#print obstacleDataJson
			#print (isObstacleDetected)
		
		
		previousObstacleDataSequenceNum = obstacleDataSequenceNum 
	except ValueError as e:
		print "error parsing obstacle info"
		pass



if isObstacleDetected:
	print "Obstacle detected"

	#print obstaclePosition 
	turningLeft = 0 
	turningRight = 0
	if obstaclePosition  == SENSOR_POS_UPPER_FRONT_CENTER or obstaclePosition  == SENSOR_POS_LOWER_FRONT_CENTER or obstaclePosition  == SENSOR_POS_MIDDLE_CENTER:
		
		directionData['direction'] = DIRECTION_STOP
		directionData['seq_num'] = imageCount 
		directionDataJson = json.dumps(directionData, separators=(',', ':'))
		rr.SetVariable("DIRECTION_DATA", directionDataJson+'#')
			
		if (not isWelcomeVoicePlaying) and (previousObstaclePosition != obstaclePosition):
			
			
			previousObstacleDetectionState = isObstacleDetected
			

			mixer.music.fadeout(SONG_FADEOUT_MILLIS)
			
			currentSongPosition = rr.GetVariable("CURRENT_SONG_POSITION")
			currentSongPosition = currentSongPosition + int(mixer.music.get_pos() / 1000)
		
			rr.SetVariable("CURRENT_SONG_POSITION", currentSongPosition)
			
			print "play welcome voice"
			isSongPlaying = 0
			mixer.music.load(WELCOME_VOICE_PATH)
			mixer.music.play()
			isWelcomeVoicePlaying = 1
			lastWelcomeVoicePlayedTime = clockTime
			
		isObstacleOnFront = 1
		turningLeftTowardsObstacle = 0
		turningRightTowardsObstacle = 0
		lastObstacleDetectTime = clockTime
	else:
		isObstacleOnFront = 0
		if (obstaclePosition  == SENSOR_POS_UPPER_FRONT_LEFT) or (obstaclePosition  == SENSOR_POS_UPPER_BACK_LEFT) or (obstaclePosition  == SENSOR_POS_UPPER_BACK_CENTER) or (obstaclePosition  == SENSOR_POS_LOWER_FRONT_LEFT_1) or (obstaclePosition  == SENSOR_POS_LOWER_FRONT_LEFT_2) or (obstaclePosition  == SENSOR_POS_LOWER_BACK_LEFT) or (obstaclePosition  == SENSOR_POS_LOWER_BACK_CENTER) or (obstaclePosition  == SENSOR_POS_MIDDLE_LEFT):
			if not turningRightTowardsObstacle and not isObstacleOnFront :
				directionData['direction'] = DIRECTION_LEFT_TOWARDS_OBSTACLE
				if not turningLeftTowardsObstacle:
					turnTowardsObstacleStartTime = clockTime
					turningLeftTowardsObstacle = 1
				#isObstacleOnFront = 0
				#turningRightTowardsObstacle = 0
			

		elif (obstaclePosition  == SENSOR_POS_UPPER_FRONT_RIGHT) or (obstaclePosition  == SENSOR_POS_UPPER_BACK_RIGHT) or (obstaclePosition  == SENSOR_POS_LOWER_FRONT_RIGHT_1) or (obstaclePosition  == SENSOR_POS_LOWER_FRONT_RIGHT_2) or (obstaclePosition  == SENSOR_POS_LOWER_BACK_RIGHT) or (obstaclePosition  == SENSOR_POS_MIDDLE_RIGHT):
			if not turningLeftTowardsObstacle and not isObstacleOnFront :

				directionData['direction'] = DIRECTION_RIGHT_TOWARDS_OBSTACLE
			
				if not turningRightTowardsObstacle:
					turnTowardsObstacleStartTime = clockTime
					turningRightTowardsObstacle = 1
				#isObstacleOnFront = 0
				#turningLeftTowardsObstacle = 0
		#rr.SetVariable("PREVIOUS_OBSTACLE_POS", obstaclePosition );
else:
	isObstacleOnFront = 0
	

	#rr.SetVariable("PREVIOUS_OBSTACLE_POS", "NA" );	

	if previousObstacleDetectionState == 1 and (previousObstaclePosition == SENSOR_POS_UPPER_FRONT_CENTER or previousObstaclePosition == SENSOR_POS_LOWER_FRONT_CENTER or previousObstaclePosition == SENSOR_POS_MIDDLE_CENTER):
		if not mixer.music.get_busy():
			isSongPlaying = 0
			#isThankYouVoicePlaying = 1
			mixer.init()
			mixer.music.load(THANK_YOU_AUDIO_PATH)
			mixer.music.play()
			rr.Sleep(2000)
			#isThankYouVoicePlaying = 0
	
		else:
			mixer.music.fadeout(SONG_FADEOUT_MILLIS)
		
		currentSongPosition = rr.GetVariable("CURRENT_SONG_POSITION")
		print currentSongPosition
		mixer.music.load(SONG_PATH)
		#mixer.music.set_pos(currentSongPosition)
		if currentSongPosition == -1:
			currentSongPosition = 0
		mixer.music.play(0, currentSongPosition)
		
	isSongPlaying = 1


if isSongPlaying:
	currentRelativePosition = mixer.music.get_pos()
	print currentRelativePosition
	if currentRelativePosition == -1:
		rr.SetVariable("CURRENT_SONG_POSITION", 0)
		mixer.music.load(SONG_PATH)
		mixer.music.play()


#songStarted = 0	
#mixer.music.stop()

if (clockTime - lastSerialInputReceivedTime) > MAX_SERIAL_INPUT_TIMEOUT :
	isObstacleDetected = 0;
	
if (clockTime - lastWelcomeVoicePlayedTime) > MAX_AUDIO_PLAYBACK_TIME :
	isWelcomeVoicePlaying = 0
	#isThankYouVoicePlaying = 0
	



if (turningLeftTowardsObstacle or turningRightTowardsObstacle) and (clockTime - turnTowardsObstacleStartTime) > 8:
	turningLeftTowardsObstacle = 0
	turningRightTowardsObstacle = 0
	print "Stop obstacle turn timeout"
	directionData['direction'] = DIRECTION_STOP

#print objectYCoord
#print prevObjectYCoord 
if objectYCoord != prevObjectYCoord or isObstacleDetected:
	#print "updating coord"
	lastCoordUpdateTime = clockTime 

#if (clockTime - lastCoordUpdateTime) >= 10:
	#print "Coordinates not updated for more than 10 secs, stop now"
	
	#directionData['direction'] = DIRECTION_STOP

if isSensorDefective:
	print "Sensor is Defective stop!"
	directionData['direction'] = DIRECTION_STOP

prevObjectXCoord = objectXCoord
prevObjectYCoord = objectYCoord

directionData['seq_num'] = imageCount 
previousObstacleDetectionState = isObstacleDetected
directionDataJson = json.dumps(directionData, separators=(',', ':'))
print (directionDataJson)



rr.SetVariable("DIRECTION_DATA", directionDataJson+'#')
rr.SetVariable("PREVIOUS_DIRECTION", directionData['direction'])
rr.SetVariable("IS_TURNING_RIGHT", turningRight)
rr.SetVariable("IS_TURNING_LEFT", turningLeft)
rr.SetVariable("IS_TURNING_BACK", turningBack);
rr.SetVariable("IS_TURNING_LEFT_TOWARDS_OBSTACLE", turningLeftTowardsObstacle);
rr.SetVariable("IS_TURNING_RIGHT_TOWARDS_OBSTACLE", turningRightTowardsObstacle);
rr.SetVariable("IS_OBSTACLE_ON_FRONT", isObstacleOnFront);
rr.SetVariable("TURN_TOWARDS_OBSTACLE_START_TIME", turnTowardsObstacleStartTime);
rr.SetVariable("TARGET_TURN_POSITION", targetTurnPosition)
rr.SetVariable("LAST_OBJECT_TRACKED_TIME", lastObjectTrackedTime)
rr.SetVariable("TURN_STATUS_JSON", turnStatusJson);
rr.SetVariable("PREV_TURN_STATUS_SEQ_NUM", previousTurnStatusSequenceNum);
rr.SetVariable("TURN_COUNT", turnCount);
rr.SetVariable("OBSTACLE_STATUS_JSON", obstacleStatusData);
rr.SetVariable("PREV_OBSTACLE_DATA_SEQ_NUM", previousObstacleDataSequenceNum);
rr.SetVariable("PREV_OBSTACLE_DETECT_STATE", previousObstacleDetectionState);
rr.SetVariable("IS_OBSTACLE_DETECTED", isObstacleDetected);
rr.SetVariable("LAST_SERIAL_INPUT_RECEIVED_TIME", lastSerialInputReceivedTime);
rr.SetVariable("LAST_AUDIO_PLAYED_TIME", lastWelcomeVoicePlayedTime);
rr.SetVariable("PREV_OBJECT_X_COORD", prevObjectXCoord );
rr.SetVariable("PREV_OBJECT_Y_COORD", prevObjectYCoord );
rr.SetVariable("LAST_COORD_UPDATE_TIME", lastCoordUpdateTime);
rr.SetVariable("LAST_TURN_TIME", lastTurnTime); 
rr.SetVariable("LAST_OBSTACLE_DETECT_TIME", lastObstacleDetectTime);
rr.SetVariable("SONG_STARTED", songStarted);
rr.SetVariable("IS_WELCOME_VOICE_PLAYING", isWelcomeVoicePlaying);
rr.SetVariable("PREVIOUS_OBSTACLE_POSITION", obstaclePosition);
rr.SetVariable("IS_SENSOR_DEFECTIVE", isSensorDefective);
#rr.SetVariable("IS_THANK_YOU_PLAYING", isThankYouVoicePlaying);